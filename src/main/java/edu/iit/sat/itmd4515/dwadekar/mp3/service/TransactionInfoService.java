/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.TransactionInfo;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to provide various transaction information for a Customer
 *
 * @author dwadekar
 */
@Stateless
public class TransactionInfoService extends AbstractService<TransactionInfo> {

    /**
     * Default Constructor
     */
    public TransactionInfoService() {
        super(TransactionInfo.class);
    }

    /**
     * Returns the all information about Transactions done by Customers
     *
     * @return
     */
    @Override
    public List<TransactionInfo> findAll() {
        return em.createNamedQuery("TransactionInfo.findAll", TransactionInfo.class).getResultList();
    }
}
