/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.ProofOfDocuments;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used for providing Proof of Document service to Customer
 *
 * @author dwadekar
 */
@Stateless
public class ProofOfDocService extends AbstractService<ProofOfDocuments> {

    /**
     * Default Constructor
     */
    public ProofOfDocService() {
        super(ProofOfDocuments.class);
    }

    /**
     * Returns the all Customer's Proof of Document information
     *
     * @return
     */
    @Override
    public List<ProofOfDocuments> findAll() {
        return em.createNamedQuery("ProofOfDocuments.findAll", ProofOfDocuments.class).getResultList();
    }

}
