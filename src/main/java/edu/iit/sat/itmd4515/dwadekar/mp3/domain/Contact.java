/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Contact details of Customer domain class
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Contact_Info")
@NamedQueries({
    @NamedQuery(name = "Contact.findAll", query = "select c from Contact c")
})
public class Contact extends CommonEntity {

    private String emailAddress;
    private String mobileNumber;
    private String homeNumber;
    private String officeNumber;

    /**
     * Default constructor with no parameters for class Contact
     */
    public Contact() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param emailAddress
     * @param mobileNumber
     * @param homeNumber
     * @param officeNumber
     * @param dateOfCreation
     */
    public Contact(String emailAddress, String mobileNumber, String homeNumber, String officeNumber, Date dateOfCreation) {
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.homeNumber = homeNumber;
        this.officeNumber = officeNumber;
    }

    /**
     * Returns the Email Address set for the given object
     *
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Set the Email Address for the passed object
     *
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Returns the Mobile Number set for the given object
     *
     * @return
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Set the Mobile Number for the passed object
     *
     * @param mobileNumber
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * Returns the Home Number set for the given object
     *
     * @return
     */
    public String getHomeNumber() {
        return homeNumber;
    }

    /**
     * Set the Home Number for the passed object
     *
     * @param homeNumber
     */
    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    /**
     * Return the Office Number set for the given object
     *
     * @return
     */
    public String getOfficeNumber() {
        return officeNumber;
    }

    /**
     * Set the Office Number for the passed object
     *
     * @param officeNumber
     */
    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    @Override
    public String toString() {
        return String.format("%10s%10s%10s%11s", getEmailAddress() + " |", getMobileNumber() + " |", getHomeNumber() + " |", getOfficeNumber() + " |");
    }

}
