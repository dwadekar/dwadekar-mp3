/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Customer;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Class used for Bank Services
 *
 * @author dwadekar
 */
@Stateless
public class BankService extends AbstractService<Customer> {

    /**
     * Default COnstructor
     */
    public BankService() {
        super(Customer.class);
    }

    /**
     * Returns the all Customer information
     *
     * @return
     */
    @Override
    public List<Customer> findAll() {
        return em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
    }

    /**
     * Returns the Customer information searched by Username
     *
     * @param username
     * @return
     */
    public Customer findCustomer(String username) {
        Customer cust = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", username).getSingleResult();
        return cust;
    }
}
