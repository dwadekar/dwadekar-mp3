/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AccountInfo;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to provide account creation service to Customer
 *
 * @author dwadekar
 */
@Stateless
public class AccountInfoService extends AbstractService<AccountInfo> {

    /**
     * Default Constructor
     */
    public AccountInfoService() {
        super(AccountInfo.class);
    }

    /**
     * Returns the all information about accounts of various Customers
     *
     * @return
     */
    @Override
    public List<AccountInfo> findAll() {
        return em.createNamedQuery("AccountInfo.findAll", AccountInfo.class).getResultList();
    }
}
