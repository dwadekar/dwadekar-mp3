/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Employee;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used for providing Employee Services to Employee Group
 *
 * @author dwadekar
 */
@Stateless
public class EmployeeService extends AbstractService<Employee> {

    /**
     * Default Constructor
     */
    public EmployeeService() {
        super(Employee.class);
    }

    /**
     * Returns the list of all Employee's login information
     *
     * @return
     */
    @Override
    public List<Employee> findAll() {
        return em.createNamedQuery("Employee.findAll").getResultList();
    }

    /**
     * Return the information of Single Employee by username
     *
     * @param username
     * @return
     */
    public Employee findByUsername(String username) {
        TypedQuery<Employee> query = em.createNamedQuery("Employee.findByUsername", Employee.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

}
