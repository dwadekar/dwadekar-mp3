/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Address;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Admin;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AssignedCustomers;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Contact;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Customer;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.CustomersLogin;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Employee;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.ProofOfDocuments;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.RelationshipManager;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.TransactionInfo;
import edu.iit.sat.itmd4515.dwadekar.mp3.security.Group;
import edu.iit.sat.itmd4515.dwadekar.mp3.security.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Used to initialize database with some pre-define set of data
 *
 * @author dwadekar
 */
@Singleton
@Startup
public class StartupBean {

    @PersistenceContext(name = "dwadekarPU")
    EntityManager em;

    @EJB
    BankService bankService;

    @EJB
    EmployeeService empService;

    @EJB
    CustomerService custService;

    @EJB
    AdminService adminService;

    @EJB
    AddressService addressService;

    @EJB
    ContactService contactService;

    @EJB
    ProofOfDocService proofService;

    @EJB
    AccountInfoService accountInfoService;

    @EJB
    TransactionInfoService txnInfoService;

    @EJB
    RelationshipManagerService relationshipManagerService;

    @EJB
    AssignedCustomersService assignedCustomersService;

    /**
     * Default Constructor
     */
    public StartupBean() {
    }

    @PostConstruct
    private void postConstruct() {
        Group admin = new Group("Admin", "This group is for Admin's only");
        Group employees = new Group("Employees", "This group is for Employee's only");
        Group customers = new Group("Customers", "This group is for customers only");
        em.persist(admin);
        em.persist(employees);
        em.persist(customers);

        User employee1 = new User("Sung", "eone");
        User employee2 = new User("Michael", "etwo");
        User customer1 = new User("Remanda", "cone");
        User customer2 = new User("Akriti", "ctwo");
        User administrator = new User("admin", "admin");
        employee1.addUserToGroup(employees);
        employee2.addUserToGroup(employees);
        customer1.addUserToGroup(customers);
        customer2.addUserToGroup(customers);
        administrator.addUserToGroup(admin);
        em.persist(employee1);
        em.persist(employee2);
        em.persist(customer1);
        em.persist(customer2);
        em.persist(administrator);

        Employee e1 = new Employee();
        e1.setUser(employee1);
        Employee e2 = new Employee();
        e2.setUser(employee2);
        CustomersLogin cust1 = new CustomersLogin();
        cust1.setUser(customer1);
        CustomersLogin cust2 = new CustomersLogin();
        cust2.setUser(customer2);
        Admin admins = new Admin();
        admins.setUser(administrator);

        empService.create(e1);
        empService.create(e2);
        custService.create(cust1);
        custService.create(cust2);
        adminService.create(admins);

        //Adding customer information in database
        Customer cust = new Customer("Ms.", "Remanda", "D.", "Samuel", "Personal Account", false, 00154L, new GregorianCalendar(1985, 8, 14).getTime(), new Date());
        Address address1 = new Address("12", "Arlington House", 5, "Arthur Road", "Chicago", "Illinois", "United States", 615203, new GregorianCalendar(1985, 1, 15).getTime());
        Contact contact1 = new Contact("xyz2@gmail.com", "1-800-123458", "256488979", "35679568", new Date());
        ProofOfDocuments documentProof1 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof2 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList1 = new ArrayList<>();
        documentProofList1.add(documentProof1);
        documentProofList1.add(documentProof2);
        cust.setDocumentProof(documentProofList1);
        cust.setAddress(address1);
        cust.setContact(contact1);

        bankService.create(cust);
        addressService.create(address1);
        contactService.create(contact1);
        proofService.create(documentProof1);
        proofService.create(documentProof2);

        Customer custInfo2 = new Customer("Mrs.", "Akriti", "G.", "Tandon", "Personal Account", false, 00357L, new GregorianCalendar(1978, 4, 8).getTime(), new Date());
        Address address2 = new Address("20/7", "Bombay House", 6, "Washigton Road", "Washington", "New York", "United States", 614283, new GregorianCalendar(1993, 4, 10).getTime());
        Contact contact2 = new Contact("abc132@gmail.com", "1-320-1212458", "23545688", "32149568", new Date());
        ProofOfDocuments documentProof3 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof4 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList2 = new ArrayList<>();
        documentProofList2.add(documentProof3);
        documentProofList2.add(documentProof4);
        custInfo2.setDocumentProof(documentProofList2);
        custInfo2.setAddress(address2);
        custInfo2.setContact(contact2);

        bankService.create(custInfo2);
        addressService.create(address2);
        contactService.create(contact2);
        proofService.create(documentProof3);
        proofService.create(documentProof4);

        Customer custInfo3 = new Customer("Mr.", "Alaister", "K.", "Cook", "Personal Account", false, 00564L, new GregorianCalendar(1992, 7, 4).getTime(), new Date());
        Address address3 = new Address("2/1", "George House", 10, "Wall Road", "Dadar", "Mumbai", "India", 415878, new GregorianCalendar(1995, 10, 25).getTime());
        Contact contact3 = new Contact("lmn5@yahoo.com", "1-800-954858", "256488979", "341312568", new Date());
        ProofOfDocuments documentProof5 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof6 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList3 = new ArrayList<>();
        documentProofList3.add(documentProof5);
        documentProofList3.add(documentProof6);
        custInfo3.setDocumentProof(documentProofList3);
        custInfo3.setAddress(address3);
        custInfo3.setContact(contact3);

        bankService.create(custInfo3);
        addressService.create(address3);
        contactService.create(contact3);
        proofService.create(documentProof5);
        proofService.create(documentProof6);

        //Adding Account creation and Transaction information
        long randomNum = Math.abs(new Random().nextLong());
        AccountInfo account1 = new AccountInfo(randomNum, 1L, "Savings Account", "", 500.23F, "USD", new Date());
        TransactionInfo txnInfo11 = new TransactionInfo(1L, randomNum, "D", 150F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo21 = new TransactionInfo(1L, randomNum, "C", 25F, "RTGS Transfer", new Date());
        List<TransactionInfo> transactions = new ArrayList<>();
        transactions.add(txnInfo11);
        transactions.add(txnInfo21);
        account1.setTransactions(transactions);

        accountInfoService.create(account1);
        txnInfoService.create(txnInfo11);
        txnInfoService.create(txnInfo21);

        long randomNum2 = Math.abs(new Random().nextLong());
        AccountInfo account2 = new AccountInfo(randomNum2, 2L, "Current Account", "", 1235.23F, "USD", new Date());
        TransactionInfo txnInfo12 = new TransactionInfo(2L, randomNum2, "D", 600F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo22 = new TransactionInfo(2L, randomNum2, "C", 500F, "RTGS Transfer", new Date());
        List<TransactionInfo> transactions2 = new ArrayList<>();
        transactions2.add(txnInfo12);
        transactions2.add(txnInfo22);
        account2.setTransactions(transactions2);

        accountInfoService.create(account2);
        txnInfoService.create(txnInfo12);
        txnInfoService.create(txnInfo22);

        long randomNum3 = Math.abs(new Random().nextLong());
        AccountInfo account3 = new AccountInfo(randomNum3, 3L, "Savings Account", "", 635.23F, "USD", new Date());
        TransactionInfo txnInfo13 = new TransactionInfo(3L, randomNum3, "D", 200F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo23 = new TransactionInfo(3L, randomNum3, "C", 100F, "NEFT Transfer", new Date());
        List<TransactionInfo> transactions3 = new ArrayList<>();
        transactions3.add(txnInfo13);
        transactions3.add(txnInfo23);
        account3.setTransactions(transactions3);

        accountInfoService.create(account3);
        txnInfoService.create(txnInfo13);
        txnInfoService.create(txnInfo23);

        //Assigning Relationship Manager to various Customers
        RelationshipManager rmanager = new RelationshipManager(561L, "Mr.", "Sung", "Li");
        AssignedCustomers customers1 = new AssignedCustomers(1L, "Mr. Quinton de Kock");
        AssignedCustomers customers2 = new AssignedCustomers(2L, "Ms. R Samuel");
        List<AssignedCustomers> customersList = new ArrayList<>();
        customersList.add(customers1);
        customersList.add(customers2);
        rmanager.setCustomers(customersList);

        relationshipManagerService.create(rmanager);
        assignedCustomersService.create(customers1);
        assignedCustomersService.create(customers2);

        RelationshipManager rmanager2 = new RelationshipManager(125L, "Ms.", "Fatena", "Goshtz");
        AssignedCustomers customer12 = new AssignedCustomers(3L, "Mr. James Pattison");
        List<AssignedCustomers> assignedCustomers2 = new ArrayList<>();
        assignedCustomers2.add(customer12);
        rmanager2.setCustomers(assignedCustomers2);

        relationshipManagerService.create(rmanager2);
        assignedCustomersService.create(customer12);

        RelationshipManager rmanager3 = new RelationshipManager(26L, "Mr.", "Michael", "Bevan");
        AssignedCustomers customer13 = new AssignedCustomers(4L, "Ms. Amrita Goswami");
        AssignedCustomers customer23 = new AssignedCustomers(5L, "Ms. Akriti Tandon");
        AssignedCustomers customer33 = new AssignedCustomers(6L, "Mr. Ian Bell");
        List<AssignedCustomers> customers3 = new ArrayList<>();
        customers3.add(customer13);
        customers3.add(customer23);
        customers3.add(customer33);
        rmanager3.setCustomers(customers3);

        relationshipManagerService.create(rmanager3);
        assignedCustomersService.create(customer13);
        assignedCustomersService.create(customer23);
        assignedCustomersService.create(customer33);
    }
}
