/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.CustomersLogin;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used to providing Customer services to Customers Group
 *
 * @author dwadekar
 */
@Stateless
public class CustomerService extends AbstractService<CustomersLogin> {

    /**
     * Default Constructor
     */
    public CustomerService() {
        super(CustomersLogin.class);
    }

    /**
     * Returns the all Customer's login information
     *
     * @return
     */
    @Override
    public List<CustomersLogin> findAll() {
        return em.createNamedQuery("CustomersLogin.findAll").getResultList();
    }

    /**
     * Returns the Single Customer's login information by username
     *
     * @param username
     * @return
     */
    public CustomersLogin findByUsername(String username) {
        TypedQuery<CustomersLogin> query = em.createNamedQuery("CustomersLogin.findByUsername", CustomersLogin.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

}
