/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Address of Customer domain class
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Address_Info")
@NamedQueries({
    @NamedQuery(name = "Address.findAll", query = "select a from Address a")
})
public class Address extends CommonEntity {

    private String houseNo;
    private String houseName;
    private int buildingNo;
    private String streetName;
    private String cityName;
    private String townName;
    private String countryName;
    private int postalCode;
    @Temporal(TemporalType.DATE)
    private Date presentAddressDate;

    /**
     * Default constructor with no parameters for class Address
     */
    public Address() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param houseNo
     * @param houseName
     * @param buildingNo
     * @param streetName
     * @param cityName
     * @param townName
     * @param countryName
     * @param postalCode
     * @param presentAddressDate
     */
    public Address(String houseNo, String houseName, int buildingNo, String streetName, String cityName, String townName, String countryName, int postalCode, Date presentAddressDate) {
        this.houseNo = houseNo;
        this.houseName = houseName;
        this.buildingNo = buildingNo;
        this.streetName = streetName;
        this.cityName = cityName;
        this.townName = townName;
        this.countryName = countryName;
        this.postalCode = postalCode;
        this.presentAddressDate = presentAddressDate;
    }

    /**
     * Returns the House No set for the given object
     *
     * @return
     */
    public String getHouseNo() {
        return houseNo;
    }

    /**
     * Set the House No for passed object
     *
     * @param houseNo
     */
    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    /**
     * Returns the House Name set for the given object
     *
     * @return
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * Set the House Name for the passed object
     *
     * @param houseName
     */
    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    /**
     * Returns the Building No set for the given object
     *
     * @return
     */
    public int getBuildingNo() {
        return buildingNo;
    }

    /**
     * Set the Building No for the passed object
     *
     * @param buildingNo
     */
    public void setBuildingNo(int buildingNo) {
        this.buildingNo = buildingNo;
    }

    /**
     * Returns the Street Name set for the given object
     *
     * @return
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Set the Street Name for the passed object
     *
     * @param streetName
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Returns the City Name set for the given object
     *
     * @return
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Set the City Name for the passed object
     *
     * @param cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * Returns the Town Name set for the given object
     *
     * @return
     */
    public String getTownName() {
        return townName;
    }

    /**
     * Set the Town Name for the passed object
     *
     * @param townName
     */
    public void setTownName(String townName) {
        this.townName = townName;
    }

    /**
     * Returns the Country Name set for the given object
     *
     * @return
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Set the Country name for the passed object
     *
     * @param countryName
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * Returns the Postal Code set for the given object
     *
     * @return
     */
    public int getPostalCode() {
        return postalCode;
    }

    /**
     * Set the Postal Code for the passed object
     *
     * @param postalCode
     */
    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Returns the Present Address Date set for the given object
     *
     * @return
     */
    public Date getPresentAddressDate() {
        return presentAddressDate;
    }

    /**
     * Set the Present Address Date for the passed object
     *
     * @param presentAddressDate
     */
    public void setPresentAddressDate(Date presentAddressDate) {
        this.presentAddressDate = presentAddressDate;
    }

    @Override
    public String toString() {
        return String.format("%10s%15s%13s%17s%15s%11s%14s%13s%10s", getHouseNo() + " |", getHouseName() + " |", getBuildingNo() + " |", getStreetName() + " |", getCityName() + " |", getTownName() + " |", getCountryName() + " |", getPostalCode() + " |", getPresentAddressDate() + " |");
    }

}
