/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.web;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Address;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Admin;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AssignedCustomers;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Contact;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Customer;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.CustomersLogin;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.Employee;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.ProofOfDocuments;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.RelationshipManager;
import edu.iit.sat.itmd4515.dwadekar.mp3.domain.TransactionInfo;
import edu.iit.sat.itmd4515.dwadekar.mp3.security.Group;
import edu.iit.sat.itmd4515.dwadekar.mp3.security.User;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.AccountInfoService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.AddressService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.AdminService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.AssignedCustomersService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.BankService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.ContactService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.CustomerService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.EmployeeService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.ProofOfDocService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.RelationshipManagerService;
import edu.iit.sat.itmd4515.dwadekar.mp3.service.TransactionInfoService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Used as Servlet for the project
 *
 * @author dwadekar
 */
@WebServlet(name = "MP3Servlet", urlPatterns = {"/MP3Servlet"})
public class MP3Servlet extends HttpServlet {

    @EJB
    BankService bankService;

    @EJB
    EmployeeService empService;

    @EJB
    CustomerService custService;

    @EJB
    AdminService adminService;

    @EJB
    AddressService addressService;

    @EJB
    ContactService contactService;

    @EJB
    ProofOfDocService proofService;

    @EJB
    AccountInfoService accountInfoService;

    @EJB
    TransactionInfoService txnInfoService;

    @EJB
    RelationshipManagerService relationshipManagerService;

    @EJB
    AssignedCustomersService assignedCustomersService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MP3Servlet</title>");
            out.println("<style>");
            out.println("body {background-color:lightgrey}");
            out.println("h1   {color:blue}");
            out.println("table, th, td {");
            out.println("border: 1px solid black;");
            out.println("border-collapse: collapse;}");
            out.println("th,td {padding: 5px;}");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            //out.println("<h1>Servlet MP3Servlet at " + request.getContextPath() + "</h1>");

            if (request.isUserInRole("EMPLOYEE")) {
                Employee emp = empService.findByUsername(request.getRemoteUser());
                String username = emp.getUser().getUserName();
                out.println("<h1>Welcome Employee: " + emp.getUser().getUserName() + "</hl>");

                //Display list of All customers
                List<Customer> customers = bankService.findAll();
                out.println("<h3>All Customer's Information: </h3>");
                for (Customer cust : customers) {
                    out.println("<h4>Customer ID: " + cust.getId() + "</h4>");
                    out.println("<table style=\"width:100%\">");
                    out.println("<tr>");
                    out.println("<th>Customer ID</th>");
                    out.println("<th>Customer Name</th>");
                    out.println("<th>Account Type</th>");
                    out.println("<th>Date of Birth</th>");
                    out.println("<th>Home Branch</th>");
                    out.println("<th>Customer Address</th>");
                    out.println("<th>Customer Email Address</th>");
                    out.println("<th>Mobile No</th>");
                    out.println("<th>Documents</th>");
                    out.println("</tr>");
                    Address addressInfo = cust.getAddress();
                    Contact contactInfo = cust.getContact();
                    out.println("<tr>");
                    out.println("<td>" + cust.getId() + "</td>");
                    out.println("<td>" + cust.getTitle() + " " + cust.getFirstName() + " " + cust.getMiddleName() + " " + cust.getLastName() + "</td>");
                    out.println("<td>" + cust.getRelationShipType() + "</td>");
                    out.println("<td>" + cust.getDateOfBirth() + "</td>");
                    out.println("<td>" + cust.getBranchId() + "</td>");
                    out.println("<td>" + addressInfo.getHouseNo() + ", " + addressInfo.getHouseName() + ", " + addressInfo.getBuildingNo() + ", " + addressInfo.getStreetName() + ", " + addressInfo.getCityName() + ", " + addressInfo.getTownName() + ", " + addressInfo.getCountryName() + "-" + addressInfo.getPostalCode() + "</td>");
                    out.println("<td>" + contactInfo.getEmailAddress() + "</td>");
                    out.println("<td>" + contactInfo.getMobileNumber() + "</td>");
                    out.println("<td>");
                    out.println("<ul>");
                    for (ProofOfDocuments docs : cust.getDocumentProof()) {
                        if (docs.getStatus().equalsIgnoreCase("submitted")) {
                            out.println("<li>" + docs.getDocumentName() + " as " + docs.getDocumentType() + "</li>");
                        }
                    }
                    out.println("</ul>");
                    out.println("</td>");
                    out.println("</tr>");
                    out.println("</table>");
                }

                //Displays List of all Account's information of all Customer
                List<AccountInfo> accounts = accountInfoService.findAll();
                out.println("<h3>All Customer's Account Information: </h3>");
                for (AccountInfo accInfo : accounts) {
                    out.println("<h4>Information of Account No: " + accInfo.getAccountNo() + "</h4>");
                    out.println("<table style=\"width:100%\">");
                    out.println("<tr>");
                    out.println("<th>Account Number</th>");
                    out.println("<th>Account Type</th>");
                    out.println("<th>Account Balance</th>");
                    out.println("</tr>");
                    out.println("<tr>");
                    out.println("<td>" + accInfo.getAccountNo() + "</td>");
                    out.println("<td>" + accInfo.getAccountType() + "</td>");
                    out.println("<td>" + accInfo.getCurrency() + " " + accInfo.getAvailableBalance() + "</td>");
                    out.println("</tr>");
                    out.println("</table>");
                    out.println("<h5>Transaction Information of Account No: " + accInfo.getAccountNo() + "</h5>");
                    out.println("<table style=\"width:100%\">");
                    out.println("<tr>");
                    out.println("<th>Date of Transaction</th>");
                    out.println("<th>Type of Transaction</th>");
                    out.println("<th>Mode of Transaction</th>");
                    out.println("<th>Amount</th>");
                    out.println("</tr>");
                    for (TransactionInfo txnInformation : accInfo.getTransactions()) {
                        out.println("<tr>");
                        out.println("<td>" + txnInformation.getTxnDate() + "</td>");
                        out.println("<td>" + txnInformation.getTypeOfTxn() + "</td>");
                        out.println("<td>" + txnInformation.getModeOfTxn() + "</td>");
                        out.println("<td>" + txnInformation.getAmount() + "</td>");
                        out.println("</tr>");
                    }
                    out.println("</table>");
                }

                //Displays list of assigned Customers to respective Employee
                RelationshipManager rManager = relationshipManagerService.findManager(username);
                out.println("<h3>The following customers are asssign to " + rManager.getTitle() + " " + rManager.getFirtsName() + " " + rManager.getLastName() + " are as follows:" + "</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<th>Customer ID</th>");
                out.println("<th>Customer Name</th>");
                out.println("</tr>");
                for (AssignedCustomers customer : rManager.getCustomers()) {
                    out.println("<tr>");
                    out.println("<td>" + customer.getCustomerID() + "</td>");
                    out.println("<td>" + customer.getCustomerName() + "</td>");
                    out.println("</tr>");
                }
                out.println("</table>");
            } else if (request.isUserInRole("CUSTOMER")) {
                CustomersLogin cust = custService.findByUsername(request.getRemoteUser());
                out.println("<h1>Welcome Customer: " + cust.getUser().getUserName() + "</h1>");

                String username = cust.getUser().getUserName();
                Customer custInfo = bankService.findCustomer(username);
                Long customerID = custInfo.getId();
                Address addressInfo = custInfo.getAddress();
                Contact contactInfo = custInfo.getContact();
                List<ProofOfDocuments> docs = custInfo.getDocumentProof();
                out.println("<h3>Customer Information</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<td>Customer ID:</td>");
                out.println("<td>" + custInfo.getId() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Customer Name:</td>");
                out.println("<td>" + custInfo.getTitle() + " " + custInfo.getFirstName() + " " + custInfo.getMiddleName() + " " + custInfo.getLastName() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Account Type:</td>");
                out.println("<td>" + custInfo.getRelationShipType() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Date of Birth:</td>");
                out.println("<td>" + custInfo.getDateOfBirth() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Home Branch:</td>");
                out.println("<td>" + custInfo.getBranchId() + "</td>");
                out.println("</tr>");
                out.println("</table>");

                //Address information of Customer
                out.println("<h3>Address Information</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<td>Address Line 1:</td>");
                out.println("<td>" + addressInfo.getHouseNo() + " " + addressInfo.getHouseName() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Address Line 2:</td>");
                out.println("<td>" + addressInfo.getBuildingNo() + " " + addressInfo.getStreetName() + " " + addressInfo.getCityName() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Address Line 3:</td>");
                out.println("<td>" + addressInfo.getTownName() + " " + addressInfo.getCountryName() + "-" + addressInfo.getPostalCode() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Present Address Date:</td>");
                out.println("<td>" + addressInfo.getPresentAddressDate() + "</td>");
                out.println("</tr>");
                out.println("</table>");

                //Contact information of Customer
                out.println("<h3>Contact Information</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<td>Email Address:</td>");
                out.println("<td>" + contactInfo.getEmailAddress() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Mobile Number:</td>");
                out.println("<td>" + contactInfo.getMobileNumber() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Home Number:</td>");
                out.println("<td>" + contactInfo.getHomeNumber() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Office Number:</td>");
                out.println("<td>" + contactInfo.getOfficeNumber() + "</td>");
                out.println("</tr>");
                out.println("</table>");

                //List of documents submitted by Customer
                out.println("<h3>List of Documents submitted to Bank</h3>");
                out.println("<ul>");
                for (ProofOfDocuments docSubmitted : docs) {
                    if (docSubmitted.getStatus().equalsIgnoreCase("submitted")) {
                        out.println("<li>" + docSubmitted.getDocumentName() + " as " + docSubmitted.getDocumentType() + "</li>");
                    }
                }
                out.println("</ul>");

                //Account Information of Customer
                AccountInfo accountInfo = accountInfoService.find(customerID);
                List<TransactionInfo> txnInfo = accountInfo.getTransactions();
                out.println("<h3>Account Summary</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<td>Account Number:</td>");
                out.println("<td>" + accountInfo.getAccountNo() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Account Type:</td>");
                out.println("<td>" + accountInfo.getAccountType() + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>Account Balance:</td>");
                out.println("<td>" + accountInfo.getCurrency() + " " + accountInfo.getAvailableBalance() + "</td>");
                out.println("</tr>");
                out.println("</table>");

                //Transactions in Customer's Account
                out.println("<h3>Transaction Summary of Account: " + accountInfo.getAccountNo() + "</h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<th>Date of Transaction</th>");
                out.println("<th>Type of Transaction</th>");
                out.println("<th>Mode of Transaction</th>");
                out.println("<th>Amount</th>");
                out.println("</tr>");
                for (TransactionInfo txnInformation : txnInfo) {
                    out.println("<tr>");
                    out.println("<td>" + txnInformation.getTxnDate() + "</td>");
                    out.println("<td>" + txnInformation.getTypeOfTxn() + "</td>");
                    out.println("<td>" + txnInformation.getModeOfTxn() + "</td>");
                    out.println("<td>" + txnInformation.getAmount() + "</td>");
                    out.println("</tr>");
                }
                out.println("</table>");

            } else if (request.isUserInRole("ADMINS")) {
                Admin adminUser = adminService.findByUsername(request.getRemoteUser());
                out.println("<h1>Welcome Customer: " + adminUser.getUser().getUserName() + "</h1>");
                User user = null;
                List<Employee> employeeList = empService.findAll();
                List<CustomersLogin> customersList = custService.findAll();
                List<Admin> adminList = adminService.findAll();
                out.println("<h3>All Employee's Login Information: </h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<th>Employee Username</th>");
                out.println("<th>Group</th>");
                out.println("<th>Group Description</th>");
                out.println("</tr>");
                for (Employee emp : employeeList) {
                    user = emp.getUser();
                    List<Group> groupList = user.getUserGroups();
                    out.println("<tr>");
                    out.println("<td>" + user.getUserName() + "</td>");
                    for (Group grp : groupList) {
                        out.println("<td>" + grp.getGroupName() + "</td>");
                        out.println("<td>" + grp.getGroupDescription() + "</td>");
                    }
                    out.println("</tr>");
                }
                out.println("</table>");

                //All Customer's login information
                out.println("<h3>All Customer's Login Information: </h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<th>Customer Username</th>");
                out.println("<th>Group</th>");
                out.println("<th>Group Description</th>");
                out.println("</tr>");
                for (CustomersLogin cust : customersList) {
                    user = cust.getUser();
                    List<Group> groupList = user.getUserGroups();
                    out.println("<tr>");
                    out.println("<td>" + user.getUserName() + "</td>");
                    for (Group grp : groupList) {
                        out.println("<td>" + grp.getGroupName() + "</td>");
                        out.println("<td>" + grp.getGroupDescription() + "</td>");
                    }
                    out.println("</tr>");
                }
                out.println("</table>");

                //All Administrator's login information
                out.println("<h3>All Administrator's Login Information: </h3>");
                out.println("<table style=\"width:100%\">");
                out.println("<tr>");
                out.println("<th>Administrator Username</th>");
                out.println("<th>Group</th>");
                out.println("<th>Group Description</th>");
                out.println("</tr>");
                for (Admin admin : adminList) {
                    user = admin.getUser();
                    List<Group> groupList = user.getUserGroups();
                    out.println("<tr>");
                    out.println("<td>" + user.getUserName() + "</td>");
                    for (Group grp : groupList) {
                        out.println("<td>" + grp.getGroupName() + "</td>");
                        out.println("<td>" + grp.getGroupDescription() + "</td>");
                    }
                    out.println("</tr>");
                }
                out.println("</table>");
            } else {
                out.println("<h1>You are not in any roles.  Who are you?  Where am I?  What day is it?</h1>");
            }
            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
