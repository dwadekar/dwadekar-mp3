/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp3.service;

import edu.iit.sat.itmd4515.dwadekar.mp3.domain.AssignedCustomers;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to store information of list of Customers to whom Relationship Managers
 * are assigned
 *
 * @author dwadekar
 */
@Stateless
public class AssignedCustomersService extends AbstractService<AssignedCustomers> {

    /**
     * Default Constructor
     */
    public AssignedCustomersService() {
        super(AssignedCustomers.class);
    }

    /**
     * Returns the all information about assigned Customers
     *
     * @return
     */
    @Override
    public List<AssignedCustomers> findAll() {
        return em.createNamedQuery("AssignedCustomers.findAll", AssignedCustomers.class).getResultList();
    }
}
